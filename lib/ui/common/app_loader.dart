import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'app_colors.dart';
import 'app_design.dart';

Widget repoLoader() {
  return Shimmer.fromColors(
    baseColor: shadowBaseColor!,
    highlightColor: shadowHighlightColor!,
    enabled: true,
    child: GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          crossAxisSpacing: 3.0,
          mainAxisSpacing: 3.0,
          childAspectRatio: 5 / 2,
        ),
// crossAxisCount: 2,
        padding: EdgeInsets.all(4.0),
        itemCount: 5,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: containerElementsPadding(),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(width: 2.0, color: Colors.grey),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        width: 80,
                        height: 8.0,
                        color: containerBackgroundColorWhite,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            height: 60,
                            width: 60,
                            child: CircleAvatar(
                              backgroundColor: Colors.red,
                              radius: 60.0,
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10.0),
                          ),
                          Container(
                            width: 200,
                            height: 8.0,
                            color: containerBackgroundColorWhite,
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                      ),
                      Container(
                        width: double.infinity,
                        height: 8.0,
                        color: containerBackgroundColorWhite,
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        }),
  );
}
