import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_repository_app/ui/common/app_images.dart';

import 'app_colors.dart';

EdgeInsetsGeometry screenBodyPadding(
        {double horizontal = 8, double vertical = 3.0}) =>
    EdgeInsets.symmetric(horizontal: horizontal, vertical: vertical);

EdgeInsetsGeometry containerElementsPadding(
        {double horizontal = 5.0, double vertical = 8.0}) =>
    EdgeInsets.symmetric(horizontal: horizontal, vertical: vertical);

Widget getProfileImageFromUrl(
        {double height = 50, double width = 50, String? imageUrl}) =>
    Container(
      height: height,
      width: width,
      child: imageUrl == null
          ? CircleAvatar(
              backgroundImage: AssetImage(IMG_PROFILE),
              radius: height,
            )
          : CircleAvatar(
              //backgroundColor: bodyContainerBackgroundColor,
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: getCacheImage(imageUrl, IMG_PROFILE, height, width,
                    BoxFit.cover, BoxShape.circle),
              ),
            ),
    );

getCacheImage(String url, String errorImageUrl, double height, double width,
    BoxFit boxFit, BoxShape boxShape) {
  return Padding(
    padding: const EdgeInsets.all(5.0),
    child: CachedNetworkImage(
      height: height,
      width: width,
      imageUrl: url,
      imageBuilder: (context, imageProvider) => Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          shape: boxShape,
          image: DecorationImage(image: imageProvider, fit: boxFit),
        ),
      ),
      progressIndicatorBuilder: (context, url, downloadProgress) =>
          CircularProgressIndicator(
        value: downloadProgress.progress,
        color: shadowBaseColorButton,
        backgroundColor: shadowHighlightColorButton,
      ),
      errorWidget: (context, url, error) => CircleAvatar(
        backgroundImage: AssetImage(IMG_PROFILE),
        radius: height,
      ),
    ),
  );
}

Widget getImageFromAsset(
        {double height = 50, double width = 50, required String imagePath}) =>
    Image.asset(
      imagePath,
      height: height,
      width: width,
      fit: BoxFit.scaleDown,
    );
