import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

const Color kcPrimaryColor = Color(0xFF9600FF);
const Color kcPrimaryColorDark = Color(0xFF300151);
const Color kcDarkGreyColor = Color(0xFF1A1B1E);
const Color kcMediumGrey = Color(0xFF474A54);
const Color kcLightGrey = Color.fromARGB(255, 187, 187, 187);
const Color kcVeryLightGrey = Color(0xFFE3E3E3);
const Color kcBackgroundColor = kcDarkGreyColor;

final Color? shadowBaseColor = Colors.grey[300];
final Color? shadowHighlightColor = Colors.grey[100];

final Color containerBackgroundColorWhite = Colors.white;
final Color cancelIconColor = HexColor(("#934242"));
final Color checkoutButtonRed = HexColor("#F44236");
final Color bodyContainerBackgroundColor = HexColor("#E8E8E8");

const Color background_color = Colors.black;
const Color primaryColor = Colors.lightGreen;

final Color? shadowBaseColorButton = Colors.indigo[300];
final Color? shadowHighlightColorButton = Colors.indigo[100];
