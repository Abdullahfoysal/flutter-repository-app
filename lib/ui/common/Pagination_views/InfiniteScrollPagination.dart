import 'package:flutter/material.dart';
import 'package:flutter_repository_app/app/shared/preference/UserPrefManager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../app/core/models/page_list.dart';
import '../app_constants.dart';
import 'NoItemFoundError.dart';
import 'NoMoreButton.dart';
import 'PageErrorIndicator.dart';

class InfiniteScrollPagination extends StatefulWidget {
  final Function(int) _fetchPage;
  final _pagingController;
  final Function(dynamic) itemBuilder;
  final Widget firstPageProgressIndicatorBuilder;
  final Widget newPageProgressIndicatorBuilder;
  final String noItemFoundAlertText;

  InfiniteScrollPagination(
      this._fetchPage,
      this._pagingController,
      this.itemBuilder,
      this.firstPageProgressIndicatorBuilder,
      this.newPageProgressIndicatorBuilder,
      this.noItemFoundAlertText);

  @override
  _InfiniteScrollPaginationState createState() =>
      _InfiniteScrollPaginationState();
}

class _InfiniteScrollPaginationState extends State<InfiniteScrollPagination> {
  @override
  void initState() {
    // 3
    widget._pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey); //api call here
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageNumber) async {
    try {
      final PagedList pagedList = await widget._fetchPage(pageNumber);

      final isLastPage = pageNumber * pageSize >=
          pagedList
              .totalCount; //newPage.isLastPage(previouslyFetchedItemsCount);
      final newItems = pagedList.list;

      if (isLastPage) {
        // 3
        widget._pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageNumber + 1;
        widget._pagingController.appendPage(newItems, nextPageKey);
      }

      ///MARK: Sort by Star Count
      sortByStarCount();
    } catch (error) {
      // 4
      if (mounted && widget._pagingController != null) {
        widget._pagingController.error = error;
        //ToastNotification().error(Utility.getApiErrorMessage(error));
      }
    }
  }

  @override
  Widget build(BuildContext context) =>
      // 1
      RefreshIndicator(
        onRefresh: () => Future.sync(
            // 2
            refreshRepoList),
        // 3
        child: Scrollbar(
          thumbVisibility: true,
          child: PagedListView.separated(
            // 4

            pagingController: widget._pagingController,
            padding: const EdgeInsets.all(0),
            separatorBuilder: (context, index) => const SizedBox(
              height: 8,
            ),
            builderDelegate: PagedChildBuilderDelegate<dynamic>(
              itemBuilder: (context, item, index) => widget.itemBuilder(
                item,
              ),
              firstPageProgressIndicatorBuilder: (context) =>
                  widget.firstPageProgressIndicatorBuilder,
              newPageProgressIndicatorBuilder: (context) =>
                  widget.newPageProgressIndicatorBuilder,
              firstPageErrorIndicatorBuilder: (context) => NoItemFoundError(
                  "Something went wrong,\nPull to try again.."),
              newPageErrorIndicatorBuilder: (context) =>
                  PageErrorIndicator(widget._pagingController),
              noItemsFoundIndicatorBuilder: (context) =>
                  NoItemFoundError(widget.noItemFoundAlertText),
              noMoreItemsIndicatorBuilder: (context) =>
                  NoMoreButton(widget._pagingController, widget._fetchPage),
            ),
          ),
        ),
      );

  @override
  void dispose() {
    // 4
    super.dispose();
  }

  void refreshRepoList() async {
    String? lastRefreshedDateSting = await UserPrefManager.getLastRefreshTime();
    if (lastRefreshedDateSting != null) {
      DateTime? lastRefreshTime = DateTime.parse(lastRefreshedDateSting);
      Duration duration = DateTime.now().difference(lastRefreshTime);
      if (duration.inSeconds >= 6) {
        UserPrefManager.saveLastRefreshTime(DateTime.now());
        widget._pagingController.refresh();
      } else {
        Fluttertoast.showToast(
            msg: "Can refresh after 6 seconds",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.white,
            textColor: Colors.green,
            fontSize: 16.0);
      }
    } else {
      UserPrefManager.saveLastRefreshTime(DateTime.now());
      widget._pagingController.refresh();
    }
  }

  void sortByStarCount() async {
    bool isSortByStarCount = await UserPrefManager.getIsSortByStarCount();
    if (isSortByStarCount) {
      widget._pagingController.itemList?.sort((a, b) {
        int aStartCount = a.stargazers_count ?? 0;
        int bStartCount = b.stargazers_count ?? 0;
        if (aStartCount > bStartCount) return 0;
        return 1;
      });
    }
  }
}
