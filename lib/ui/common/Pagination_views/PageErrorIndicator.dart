import 'package:flutter/material.dart';
import 'package:flutter_repository_app/ui/common/app_colors.dart';

class PageErrorIndicator extends StatelessWidget {
  final _pagingController;
  PageErrorIndicator(this._pagingController);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          _pagingController.retryLastFailedRequest();
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Something Went Wrong",
                style: TextStyle(
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
              SizedBox(
                width: 5,
              ),
              Icon(
                Icons.refresh,
                size: 20,
                color: primaryColor,
              )
            ],
          ),
        ));
  }
}
