import 'package:flutter/material.dart';
import 'package:flutter_repository_app/ui/common/app_colors.dart';

class NoItemFoundError extends StatelessWidget {
  String noItemAlertText;
  NoItemFoundError(this.noItemAlertText);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            noItemAlertText,
            textAlign: TextAlign.center,
            style: TextStyle(color: primaryColor),
          ),
          Icon(
            Icons.refresh,
            color: primaryColor,
          ),
        ],
      ),
    );
  }
}
