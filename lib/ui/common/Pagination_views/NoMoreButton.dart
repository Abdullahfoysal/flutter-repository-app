import 'package:flutter/material.dart';
import 'package:flutter_repository_app/ui/common/app_colors.dart';

class NoMoreButton extends StatefulWidget {
  final _pagingController;
  final Function(int) _fetchPage;
  NoMoreButton(this._pagingController, this._fetchPage);

  @override
  _NoMoreButtonState createState() => _NoMoreButtonState();
}

class _NoMoreButtonState extends State<NoMoreButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text("No More",
            style: TextStyle(
                color: primaryColor,
                fontSize: 15,
                fontWeight: FontWeight.bold)),
        SizedBox(
          width: 5,
        ),
        Icon(
          Icons.arrow_upward,
          size: 20,
          color: primaryColor,
        )
      ]),
    );
  }
}
