import 'package:flutter/material.dart';
import 'package:flutter_repository_app/app/shared/utility/time_utility.dart';
import 'package:flutter_repository_app/ui/common/app_colors.dart';
import 'package:flutter_repository_app/ui/common/ui_helpers.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:stacked/stacked.dart';

import '../../common/app_design.dart';
import 'repository_details_viewmodel.dart';

class RepositoryDetailsView extends StackedView<RepositoryDetailsViewModel> {
  final RepoEntity repoEntity;
  const RepositoryDetailsView(this.repoEntity, {Key? key}) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    RepositoryDetailsViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      backgroundColor: background_color,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      viewModel.onBackPress();
                    },
                    icon: const Row(
                      children: [
                        Icon(
                          Icons.arrow_back_ios,
                          size: 30,
                          color: primaryColor,
                        ),
                        Text(
                          'Back',
                          style: TextStyle(color: primaryColor),
                        )
                      ],
                    )),
              ],
            ),
            Flexible(
              child: ListView(
                physics: AlwaysScrollableScrollPhysics(),
                children: [
                  horizontalSpaceSmall,
                  Center(
                    child: Text(
                      "${repoEntity.name}",
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                  horizontalSpaceSmall,
                  getProfileImageFromUrl(
                      height: 200,
                      width: 200,
                      imageUrl: repoEntity.owner?.avatar_url),
                  verticalSpaceSmall,
                  Center(
                    child: Text(
                      "Full Name: ${repoEntity.full_name ?? " "}",
                      style: TextStyle(color: primaryColor),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.star,
                        color: primaryColor,
                      ),
                      Text("${repoEntity.stargazers_count ?? " - "}",
                          style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      horizontalSpaceSmall,
                      const Icon(
                        Icons.remove_red_eye,
                        color: primaryColor,
                      ),
                      Text("${repoEntity.watchers_count ?? " - "}",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                    ],
                  ),
                  verticalSpaceTiny,
                  /* Center(
                    child: Text(
                      "Description",
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),*/
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        repoEntity.description ?? "",
                        style: TextStyle(color: primaryColor),
                      ),
                    ),
                  ),
                  verticalSpaceTiny,
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Last updated: ${TimeUtility.edMYHMformatDate(repoEntity.updated_at)}",
                        style: TextStyle(color: primaryColor),
                      ),
                    ),
                  ),
                  Row(
                    children: [],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  RepositoryDetailsViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      RepositoryDetailsViewModel();
}
