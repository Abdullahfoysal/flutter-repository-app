import 'package:flutter_repository_app/app/app.locator.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

@injectable
class RepositoryDetailsViewModel extends BaseViewModel {
  final _navigationService = locator<NavigationService>();
  void onBackPress() {
    _navigationService.back();
  }
}
