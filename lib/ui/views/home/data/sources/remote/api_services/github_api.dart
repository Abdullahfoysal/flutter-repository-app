import 'package:dio/dio.dart';
import 'package:flutter_repository_app/ui/views/home/data/models/flutter_repos_response_model.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'github_api.g.dart';

@injectable
@RestApi()
abstract class GithubApi {
  @factoryMethod
  factory GithubApi(Dio dio, {@Named("baseURL") String baseUrl}) = _GithubApi;
  @GET("/search/repositories")
  Future<FlutterReposResponseModel> getGithubPaginatedRepos(
    @Query("q") String featureName,
    @Query("per_page") String pageSize,
    @Query("page") String pageNumber,
  );
}
