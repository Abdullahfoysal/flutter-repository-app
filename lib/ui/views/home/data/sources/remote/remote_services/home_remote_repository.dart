import 'package:result_monad/result_monad.dart';

import '../../../../../../../app/core/models/error.dart';
import '../../../../../../../app/core/models/page_list.dart';
import '../../../../domain/entities/repo_entity.dart';
import '../../../../params/repo_query_param.dart';

abstract class HomeRemoteRepository {
  ///MARK:- REMOTE
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getRemoteGithubPaginatedRepos(RepoQueryParam param);
}
