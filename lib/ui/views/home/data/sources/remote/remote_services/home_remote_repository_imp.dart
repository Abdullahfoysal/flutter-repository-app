import 'package:flutter_repository_app/app/core/models/page_list.dart';
import 'package:flutter_repository_app/ui/views/home/data/sources/remote/api_services/github_api.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:flutter_repository_app/ui/views/home/params/repo_query_param.dart';
import 'package:injectable/injectable.dart';
import 'package:result_monad/result_monad.dart';

import '../../../../../../../app/core/models/error.dart';
import 'home_remote_repository.dart';

@Injectable(as: HomeRemoteRepository)
class HomeRemoteRepositoryImp implements HomeRemoteRepository {
  final GithubApi _githubApi;

  HomeRemoteRepositoryImp(this._githubApi);

  @override
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getRemoteGithubPaginatedRepos(RepoQueryParam param) async {
    try {
      var response = await _githubApi.getGithubPaginatedRepos(
          param.featureName, param.pageSize, param.pageNumber);
      List<RepoEntity> repoList =
          response.items?.map((repoModel) => repoModel.toEntity()).toList() ??
              [];
      PagedList<RepoEntity> pagedList =
          PagedList(repoList, response.total_count ?? 0);
      return Result.ok(pagedList);
    } catch (e) {
      return Result.error(const ErrorModel(message: "error"));
    }
  }
}
