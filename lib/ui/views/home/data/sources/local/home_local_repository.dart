import 'package:result_monad/result_monad.dart';

import '../../../../../../app/core/models/error.dart';
import '../../../../../../app/core/models/page_list.dart';
import '../../../domain/entities/repo_entity.dart';

abstract class HomeLocalRepository {
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getLocalGithubPaginatedRepos(String pageNumber);

  Future<void> insertRepositoryEntryList(
      List<RepoEntity> list, String page_number);
}
