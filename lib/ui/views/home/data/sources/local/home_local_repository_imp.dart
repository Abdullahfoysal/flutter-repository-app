import 'package:flutter_repository_app/app/core/models/error.dart';
import 'package:flutter_repository_app/app/core/models/page_list.dart';
import 'package:flutter_repository_app/app/shared/local/app_database.dart';
import 'package:flutter_repository_app/app/shared/local/table_entity/repository_table/repository_entity_table.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:injectable/injectable.dart';
import 'package:result_monad/src/result_monad_base.dart';

import '../../../../../../app/shared/preference/UserPrefManager.dart';
import 'home_local_repository.dart';

@Injectable(as: HomeLocalRepository)
class HomeLocalRepositoryImp implements HomeLocalRepository {
  final AppDatabase _appDatabase;
  HomeLocalRepositoryImp(this._appDatabase);
  @override
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getLocalGithubPaginatedRepos(String pageNumber) async {
    try {
      var response = await _appDatabase.repositoryTableDao
          .getPaginatedGithubRepos(pageNumber);
      List<RepoEntity> repoList = response
              .map((tableEntityModel) => tableEntityModel.toEntity())
              .toList() ??
          [];
      int totalCount = await UserPrefManager.getLatestRepoCount();
      PagedList<RepoEntity> pagedList = PagedList(repoList, totalCount);
      return Result.ok(pagedList);
    } catch (e) {
      return Result.error(const ErrorModel(message: "error"));
    }
  }

  @override
  Future<void> insertRepositoryEntryList(
      List<RepoEntity> list, String page_number) {
    try {
      List<RepositoryEntityTable> repoTableEntityList =
          list.map((e) => e.toEntityTable(page_number)).toList();
      return _appDatabase.repositoryTableDao
          .insertRepositoryEntryList(repoTableEntityList);
    } catch (e) {
      return Future.value();
    }
  }
}
