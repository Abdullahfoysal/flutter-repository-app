// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flutter_repos_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FlutterReposResponseModel _$FlutterReposResponseModelFromJson(
        Map<String, dynamic> json) =>
    FlutterReposResponseModel()
      ..total_count = json['total_count'] as int?
      ..incomplete_results = json['incomplete_results'] as bool?
      ..items = (json['items'] as List<dynamic>?)
          ?.map((e) => ItemModel.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$FlutterReposResponseModelToJson(
        FlutterReposResponseModel instance) =>
    <String, dynamic>{
      'total_count': instance.total_count,
      'incomplete_results': instance.incomplete_results,
      'items': instance.items,
    };
