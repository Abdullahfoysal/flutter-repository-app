import 'package:flutter_repository_app/ui/views/home/data/models/item_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'flutter_repos_response_model.g.dart';

@JsonSerializable()
class FlutterReposResponseModel {
  int? total_count;
  bool? incomplete_results;
  List<ItemModel>? items;

  FlutterReposResponseModel();
  factory FlutterReposResponseModel.fromJson(Map<String, dynamic> json) {
    return _$FlutterReposResponseModelFromJson(json);
  }
  toJson() => _$FlutterReposResponseModelToJson(this);
}
