import 'package:json_annotation/json_annotation.dart';

import '../../domain/entities/repo_entity.dart';
import 'licence_model.dart';
import 'owner_model.dart';

part 'item_model.g.dart';

@JsonSerializable()
class ItemModel {
  int? id;
  String? node_id;
  String? name;
  String? full_name;
  bool? private;
  OwnerModel? owner;
  String? html_url;
  String? description;
  bool? fork;
  String? url;
  String? forks_url;
  String? keys_url;
  String? collaborators_url;
  String? teams_url;
  String? hooks_url;
  String? issue_events_url;
  String? events_url;
  String? assignees_url;
  String? branches_url;
  String? tags_url;
  String? blobs_url;
  String? git_tags_url;
  String? git_refs_url;
  String? trees_url;
  String? statuses_url;
  String? languages_url;
  String? stargazers_url;
  String? contributors_url;
  String? subscribers_url;
  String? subscription_url;
  String? commits_url;
  String? git_commits_url;
  String? comments_url;
  String? issue_comment_url;
  String? contents_url;
  String? compare_url;
  String? merges_url;
  String? archive_url;
  String? downloads_url;
  String? issues_url;
  String? pulls_url;
  String? milestones_url;
  String? notifications_url;
  String? labels_url;
  String? releases_url;
  String? deployments_url;
  String? created_at;
  String? updated_at;
  String? pushed_at;
  String? git_url;
  String? ssh_url;
  String? clone_url;
  String? svn_url;
  String? homepage;
  int? size;
  int? stargazers_count;
  int? watchers_count;
  String? language;
  bool? has_issues;
  bool? has_projects;
  bool? has_downloads;
  bool? has_wiki;
  bool? has_pages;
  bool? has_discussions;
  int? forks_count;
  String? mirror_url;
  bool? archived;
  bool? disabled;
  int? open_issues_count;
  LicenseModel? license;
  bool? allow_forking;
  bool? web_commit_signoff_required;
  List<String>? topics;
  String? visibility;
  int? forks;
  int? open_issues;
  int? watchers;
  String? default_branch;
  double? score;

  ItemModel();
  factory ItemModel.fromJson(Map<String, dynamic> json) {
    return _$ItemModelFromJson(json);
  }
  toJson() => _$ItemModelToJson(this);

  RepoEntity toEntity() {
    return RepoEntity(
        id,
        node_id,
        name,
        full_name,
        private,
        owner,
        html_url,
        description,
        fork,
        url,
        forks_url,
        keys_url,
        collaborators_url,
        teams_url,
        hooks_url,
        issue_events_url,
        events_url,
        assignees_url,
        branches_url,
        tags_url,
        blobs_url,
        git_tags_url,
        git_refs_url,
        trees_url,
        statuses_url,
        languages_url,
        stargazers_url,
        contributors_url,
        subscribers_url,
        subscription_url,
        commits_url,
        git_commits_url,
        comments_url,
        issue_comment_url,
        contents_url,
        compare_url,
        merges_url,
        archive_url,
        downloads_url,
        issues_url,
        pulls_url,
        milestones_url,
        notifications_url,
        labels_url,
        releases_url,
        deployments_url,
        created_at,
        updated_at,
        pushed_at,
        git_url,
        ssh_url,
        clone_url,
        svn_url,
        homepage,
        size,
        stargazers_count,
        watchers_count,
        language,
        has_issues,
        has_projects,
        has_downloads,
        has_wiki,
        has_pages,
        has_discussions,
        forks_count,
        mirror_url,
        archived,
        disabled,
        open_issues_count,
        // license,
        allow_forking,
        web_commit_signoff_required,
        topics,
        visibility,
        forks,
        open_issues,
        watchers,
        default_branch,
        score);
  }
}
