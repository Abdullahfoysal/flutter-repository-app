import 'package:json_annotation/json_annotation.dart';

part 'licence_model.g.dart';

@JsonSerializable()
class LicenseModel {
  String? key;
  String? name;
  String? spdx_id;
  String? url;
  String? node_id;

  LicenseModel();

  factory LicenseModel.fromJson(Map<String, dynamic> json) {
    return _$LicenseModelFromJson(json);
  }
  toJson() => _$LicenseModelToJson(this);
}
