import 'package:injectable/injectable.dart';
import 'package:result_monad/result_monad.dart';

import '../../../../../app/core/models/error.dart';
import '../../../../../app/core/models/page_list.dart';
import '../../domain/entities/repo_entity.dart';
import '../../domain/repository/home_repository.dart';
import '../../params/repo_query_param.dart';
import '../sources/local/home_local_repository.dart';
import '../sources/remote/remote_services/home_remote_repository.dart';

@Injectable(as: HomeRepository)
class HomeRepositoryImp implements HomeRepository {
  final HomeRemoteRepository _homeRemote;
  final HomeLocalRepository _homeLocal;

  HomeRepositoryImp(this._homeRemote, this._homeLocal);
  @override
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getRemoteGithubPaginatedRepos(RepoQueryParam param) {
    return _homeRemote.getRemoteGithubPaginatedRepos(param);
  }

  @override
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getLocalGithubPaginatedRepos(String pageNumber) {
    return _homeLocal.getLocalGithubPaginatedRepos(pageNumber);
  }

  @override
  Future<void> insertRepositoryTableEntryList(
      List<RepoEntity> list, String page_number) {
    return _homeLocal.insertRepositoryEntryList(list, page_number);
  }
}
