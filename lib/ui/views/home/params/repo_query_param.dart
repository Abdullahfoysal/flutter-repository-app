final class RepoQueryParam {
  final String featureName;
  final String pageSize;
  final String pageNumber;
  RepoQueryParam(
      {required this.featureName,
      required this.pageSize,
      required this.pageNumber});
}
