import '../domain/entities/repo_entity.dart';

final class RepoSaveToLocalParam {
  List<RepoEntity> list;
  String page_number;
  RepoSaveToLocalParam({required this.list, required this.page_number});
}
