import 'package:flutter/material.dart';
import 'package:flutter_repository_app/ui/common/app_colors.dart';
import 'package:stacked/stacked.dart';

import '../../../../../app/di/app_di_module.dart';
import '../../../../common/Pagination_views/InfiniteScrollPagination.dart';
import '../viewModels/home_viewmodel.dart';

class HomeView extends StackedView<HomeViewModel> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    HomeViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      backgroundColor: background_color,
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Container(
                color: background_color,
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Flutter Repositories",
                        style: TextStyle(
                            color: primaryColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: InfiniteScrollPagination(
                    viewModel.fetchPageFromLocal,
                    viewModel.pagingController,
                    viewModel.singleRepoItemView,
                    viewModel.firstPageProgressIndicator,
                    viewModel.newPageProgressIndicator,
                    "No Repository Available"),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: background_color.withOpacity(0.5),
          onPressed: () {
            viewModel.sortByStarCount();
          },
          label: Row(
            children: [
              Icon(
                Icons.sort,
                color: primaryColor,
              ),
              Text(
                "Sort by star",
                style: TextStyle(color: Colors.white),
              ),
            ],
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  @override
  HomeViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      getIt<HomeViewModel>();
}
