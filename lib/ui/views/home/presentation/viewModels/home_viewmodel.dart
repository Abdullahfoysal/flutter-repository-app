import 'package:flutter/material.dart';
import 'package:flutter_repository_app/app/app.bottomsheets.dart';
import 'package:flutter_repository_app/app/app.locator.dart';
import 'package:flutter_repository_app/app/app.router.dart';
import 'package:flutter_repository_app/app/shared/preference/UserPrefManager.dart';
import 'package:flutter_repository_app/app/shared/utility/time_utility.dart';
import 'package:flutter_repository_app/ui/common/app_colors.dart';
import 'package:flutter_repository_app/ui/common/app_strings.dart';
import 'package:flutter_repository_app/ui/common/ui_helpers.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:flutter_repository_app/ui/views/home/domain/usecases/remote/get_paginated_repos_from_server_usecase.dart';
import 'package:flutter_repository_app/ui/views/home/params/repo_query_param.dart';
import 'package:flutter_repository_app/ui/views/home/params/repo_save_to_local_param.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../../../app/core/models/page_list.dart';
import '../../../../common/app_constants.dart';
import '../../../../common/app_design.dart';
import '../../../../common/app_loader.dart';
import '../../domain/usecases/local/get_local_paginated_repos_usecase.dart';
import '../../domain/usecases/local/save_local_paginated_repos_usecase.dart';

@injectable
class HomeViewModel extends BaseViewModel {
  final GetPaginatedReposFromServerUsecase _getPaginatedReposFromServerUsecase;
  final GetLocalPaginatedReposUsecase _getLocalPaginatedReposUsecase;
  final SaveLocalPaginatedReposUsecase _saveLocalPaginatedReposUsecase;
  HomeViewModel(
    this._getPaginatedReposFromServerUsecase,
    this._getLocalPaginatedReposUsecase,
    this._saveLocalPaginatedReposUsecase,
  );

  final pagingController = PagingController<int, RepoEntity>(
    // 2
    firstPageKey: 1,
  );

  Future<PagedList<RepoEntity>> fetchPageFromLocal(int page) async {
    PagedList<RepoEntity> pagedList = PagedList([], 0);

    var response = await _getLocalPaginatedReposUsecase.invoke("$page");
    if (response.isSuccess) {
      pagedList = response.value;

      if (pagedList.list.isNotEmpty) {
        ///MARK:- GET FROM LOCAL
        return pagedList;
      } else {
        ///MARK:- GET FROM REMOTE
        pagedList = await fetchPageFromRemote(page);
      }
    } else {
      ///TODO:- HANDLE ERROR
    }

    ///removed prev. items here
    if (pagingController.itemList != null) {
      pagingController.itemList?.removeWhere((existingItem) {
        bool isExist = false;
        pagedList.list.forEach((newItem) {
          if (existingItem.id == newItem.id) {
            isExist = true;
          }
        });

        return isExist;
      });
    }

    return pagedList;
  }

  Future<PagedList<RepoEntity>> fetchPageFromRemote(int page) async {
    RepoQueryParam repoQueryParam = RepoQueryParam(
        featureName: "Flutter", pageSize: "$pageSize", pageNumber: "$page");
    // int totalCount = await UserPrefManager.getLatestRepoCount();
    PagedList<RepoEntity> pagedList = PagedList([], 0);

    var response =
        await _getPaginatedReposFromServerUsecase.invoke(repoQueryParam);
    if (response.isSuccess) {
      pagedList = response.value;

      ///MARK:- save to local with preference paged->total item count
      savePaginatedReposToLocal(
          RepoSaveToLocalParam(list: pagedList.list, page_number: "$page"),
          pagedList.totalCount);
    } else {
      ///TODO:- HANDLE ERROR
    }

    ///removed prev. items here
    if (pagingController.itemList != null) {
      pagingController.itemList?.removeWhere((existingItem) {
        bool isExist = false;
        pagedList.list.forEach((newItem) {
          if (existingItem.id == newItem.id) {
            isExist = true;
          }
        });

        return isExist;
      });
    }

    return pagedList;
  }

  void savePaginatedReposToLocal(
      RepoSaveToLocalParam repoSaveToLocalParam, int totalCount) {
    UserPrefManager.saveLatestTotalRepoCount(totalCount);
    _saveLocalPaginatedReposUsecase.invoke(repoSaveToLocalParam);
  }

  void sortByStarCount() async {
    bool isSortByStarCount = await UserPrefManager.getIsSortByStarCount();
    if (isSortByStarCount == false) {
      bool isSortByStarCount =
          await UserPrefManager.saveIsSortByStarCount(true);

      pagingController.itemList?.sort((a, b) {
        int aStartCount = a.stargazers_count ?? 0;
        int bStartCount = b.stargazers_count ?? 0;
        if (aStartCount > bStartCount) return 0;
        return 1;
      });
      rebuildUi();
    }
  }

  final _dialogService = locator<DialogService>();
  final _bottomSheetService = locator<BottomSheetService>();
  final _navigationService = locator<NavigationService>();

  void incrementCounter() {
    //_counter++;
    rebuildUi();
  }

  void showBottomSheet() {
    _bottomSheetService.showCustomSheet(
      variant: BottomSheetType.notice,
      title: ksHomeBottomSheetTitle,
      description: ksHomeBottomSheetDescription,
    );
  }

  Widget get firstPageProgressIndicator {
    return Container(height: 1000, child: repoLoader());
  }

  Widget get newPageProgressIndicator {
    return Container(height: 100, child: repoLoader());
  }

/*  Future<void> onRefreshInvoiceTask() async {
    pagingController.refresh();
  }*/

  singleRepoItemView(repoEntity) {
    RepoEntity repo = repoEntity as RepoEntity;
    return Padding(
      padding: screenBodyPadding(),
      child: InkWell(
        onTap: () {
          _navigationService.navigateToRepositoryDetailsView(repoEntity: repo);
        },
        child: Card(
          color: Colors.green.withOpacity(0.3),
          elevation: 1.5,
          child: Container(
            padding: containerElementsPadding(),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text(repo.name ?? "-",
                          style: const TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                    ),
                    horizontalSpaceSmall,
                    const Icon(
                      Icons.star,
                      color: primaryColor,
                    ),
                    Text("${repo.stargazers_count ?? " - "}",
                        style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    horizontalSpaceSmall,
                    const Icon(
                      Icons.remove_red_eye,
                      color: primaryColor,
                    ),
                    Text("${repo.watchers_count ?? " - "}",
                        style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                  ],
                ),
                Row(
                  children: [
                    getProfileImageFromUrl(
                        height: 80,
                        width: 80,
                        imageUrl: repo.owner?.avatar_url),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Wrap(
                              children: [
                                Text(repo.description ?? "-",
                                    style: TextStyle(color: primaryColor)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.grey,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                        "updated: ${TimeUtility.edMYHMformatDate(repoEntity.updated_at)}",
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    pagingController.dispose();
    super.dispose();
  }
}
