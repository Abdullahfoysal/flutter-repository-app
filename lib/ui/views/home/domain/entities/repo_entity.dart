import 'package:flutter_repository_app/app/shared/local/table_entity/repository_table/repository_entity_table.dart';
import 'package:flutter_repository_app/ui/views/home/data/models/owner_model.dart';

class RepoEntity {
  int? id;
  String? node_id;
  String? name;
  String? full_name;
  bool? private;
  OwnerModel? owner;
  String? html_url;
  String? description;
  bool? fork;
  String? url;
  String? forks_url;
  String? keys_url;
  String? collaborators_url;
  String? teams_url;
  String? hooks_url;
  String? issue_events_url;
  String? events_url;
  String? assignees_url;
  String? branches_url;
  String? tags_url;
  String? blobs_url;
  String? git_tags_url;
  String? git_refs_url;
  String? trees_url;
  String? statuses_url;
  String? languages_url;
  String? stargazers_url;
  String? contributors_url;
  String? subscribers_url;
  String? subscription_url;
  String? commits_url;
  String? git_commits_url;
  String? comments_url;
  String? issue_comment_url;
  String? contents_url;
  String? compare_url;
  String? merges_url;
  String? archive_url;
  String? downloads_url;
  String? issues_url;
  String? pulls_url;
  String? milestones_url;
  String? notifications_url;
  String? labels_url;
  String? releases_url;
  String? deployments_url;
  String? created_at;
  String? updated_at;
  String? pushed_at;
  String? git_url;
  String? ssh_url;
  String? clone_url;
  String? svn_url;
  String? homepage;
  int? size;
  int? stargazers_count;
  int? watchers_count;
  String? language;
  bool? has_issues;
  bool? has_projects;
  bool? has_downloads;
  bool? has_wiki;
  bool? has_pages;
  bool? has_discussions;
  int? forks_count;
  String? mirror_url;
  bool? archived;
  bool? disabled;
  int? open_issues_count;
  // LicenseModel? license;
  bool? allow_forking;
  bool? web_commit_signoff_required;
  List<String>? topics;
  String? visibility;
  int? forks;
  int? open_issues;
  int? watchers;
  String? default_branch;
  double? score;

  RepoEntity(
      this.id,
      this.node_id,
      this.name,
      this.full_name,
      this.private,
      this.owner,
      this.html_url,
      this.description,
      this.fork,
      this.url,
      this.forks_url,
      this.keys_url,
      this.collaborators_url,
      this.teams_url,
      this.hooks_url,
      this.issue_events_url,
      this.events_url,
      this.assignees_url,
      this.branches_url,
      this.tags_url,
      this.blobs_url,
      this.git_tags_url,
      this.git_refs_url,
      this.trees_url,
      this.statuses_url,
      this.languages_url,
      this.stargazers_url,
      this.contributors_url,
      this.subscribers_url,
      this.subscription_url,
      this.commits_url,
      this.git_commits_url,
      this.comments_url,
      this.issue_comment_url,
      this.contents_url,
      this.compare_url,
      this.merges_url,
      this.archive_url,
      this.downloads_url,
      this.issues_url,
      this.pulls_url,
      this.milestones_url,
      this.notifications_url,
      this.labels_url,
      this.releases_url,
      this.deployments_url,
      this.created_at,
      this.updated_at,
      this.pushed_at,
      this.git_url,
      this.ssh_url,
      this.clone_url,
      this.svn_url,
      this.homepage,
      this.size,
      this.stargazers_count,
      this.watchers_count,
      this.language,
      this.has_issues,
      this.has_projects,
      this.has_downloads,
      this.has_wiki,
      this.has_pages,
      this.has_discussions,
      this.forks_count,
      this.mirror_url,
      this.archived,
      this.disabled,
      this.open_issues_count,
      // this.license,
      this.allow_forking,
      this.web_commit_signoff_required,
      this.topics,
      this.visibility,
      this.forks,
      this.open_issues,
      this.watchers,
      this.default_branch,
      this.score);

  RepositoryEntityTable toEntityTable(String page_number) {
    return RepositoryEntityTable(
        id: id ?? 0,
        page_number: page_number,
        node_id: node_id,
        name: name,
        full_name: full_name,
        private: private,
        owner: owner,
        html_url: html_url,
        description: description,
        fork: fork,
        url: url,
        forks_url: forks_url,
        keys_url: keys_url,
        collaborators_url: collaborators_url,
        teams_url: teams_url,
        hooks_url: hooks_url,
        issue_events_url: issue_events_url,
        events_url: events_url,
        assignees_url: assignees_url,
        branches_url: branches_url,
        tags_url: tags_url,
        blobs_url: blobs_url,
        git_tags_url: git_tags_url,
        git_refs_url: git_refs_url,
        trees_url: trees_url,
        statuses_url: statuses_url,
        languages_url: languages_url,
        stargazers_url: stargazers_url,
        contributors_url: contributors_url,
        subscribers_url: subscribers_url,
        subscription_url: subscription_url,
        commits_url: commits_url,
        git_commits_url: git_commits_url,
        comments_url: comments_url,
        issue_comment_url: issue_comment_url,
        contents_url: contents_url,
        compare_url: compare_url,
        merges_url: merges_url,
        archive_url: archive_url,
        downloads_url: downloads_url,
        issues_url: issues_url,
        pulls_url: pulls_url,
        milestones_url: milestones_url,
        notifications_url: notifications_url,
        labels_url: labels_url,
        releases_url: releases_url,
        deployments_url: deployments_url,
        created_at: created_at,
        updated_at: updated_at,
        pushed_at: pushed_at,
        git_url: git_url,
        ssh_url: ssh_url,
        clone_url: clone_url,
        svn_url: svn_url,
        homepage: homepage,
        size: size,
        stargazers_count: stargazers_count,
        watchers_count: watchers_count,
        language: language,
        has_issues: has_issues,
        has_projects: has_projects,
        has_downloads: has_downloads,
        has_wiki: has_wiki,
        has_pages: has_pages,
        has_discussions: has_discussions,
        forks_count: forks_count,
        mirror_url: mirror_url,
        archived: archived,
        disabled: disabled,
        open_issues_count: open_issues_count,
        // license: license,
        allow_forking: allow_forking,
        web_commit_signoff_required: web_commit_signoff_required,
        visibility: visibility,
        forks: forks,
        open_issues: open_issues,
        watchers: watchers,
        default_branch: default_branch,
        score: score);
  }
}
