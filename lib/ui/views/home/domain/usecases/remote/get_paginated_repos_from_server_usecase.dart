import 'package:flutter_repository_app/app/core/models/error.dart';
import 'package:flutter_repository_app/app/core/models/page_list.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:injectable/injectable.dart';
import 'package:result_monad/result_monad.dart';

import '../../../../../../app/core/base_usecase.dart';
import '../../../params/repo_query_param.dart';
import '../../repository/home_repository.dart';

@injectable
class GetPaginatedReposFromServerUsecase extends BaseUseCaseParam<
    RepoQueryParam, Result<PagedList<RepoEntity>, ErrorModel>> {
  final HomeRepository _homeRepository;

  GetPaginatedReposFromServerUsecase(this._homeRepository);

  @override
  Future<Result<PagedList<RepoEntity>, ErrorModel>> invoke(
      RepoQueryParam params) {
    return _homeRepository.getRemoteGithubPaginatedRepos(params);
  }
}
