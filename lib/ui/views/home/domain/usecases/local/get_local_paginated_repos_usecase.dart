import 'package:injectable/injectable.dart';
import 'package:result_monad/result_monad.dart';

import '../../../../../../app/core/base_usecase.dart';
import '../../../../../../app/core/models/error.dart';
import '../../../../../../app/core/models/page_list.dart';
import '../../entities/repo_entity.dart';
import '../../repository/home_repository.dart';

@injectable
class GetLocalPaginatedReposUsecase extends BaseUseCaseParam<String,
    Result<PagedList<RepoEntity>, ErrorModel>> {
  final HomeRepository _homeRepository;

  GetLocalPaginatedReposUsecase(this._homeRepository);

  @override
  Future<Result<PagedList<RepoEntity>, ErrorModel>> invoke(String pageNumber) {
    return _homeRepository.getLocalGithubPaginatedRepos(pageNumber);
  }
}
