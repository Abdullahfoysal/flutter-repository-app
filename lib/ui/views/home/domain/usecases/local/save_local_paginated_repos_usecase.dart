import 'package:flutter_repository_app/app/core/base_usecase.dart';
import 'package:injectable/injectable.dart';

import '../../../params/repo_save_to_local_param.dart';
import '../../repository/home_repository.dart';

@injectable
class SaveLocalPaginatedReposUsecase
    extends BaseUseCaseParam<RepoSaveToLocalParam, void> {
  final HomeRepository _homeRepository;

  SaveLocalPaginatedReposUsecase(this._homeRepository);

  @override
  Future<void> invoke(RepoSaveToLocalParam params) {
    return _homeRepository.insertRepositoryTableEntryList(
        params.list, params.page_number);
  }
}
