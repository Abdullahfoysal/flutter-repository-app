import 'package:flutter_repository_app/app/core/models/error.dart';
import 'package:flutter_repository_app/app/core/models/page_list.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:flutter_repository_app/ui/views/home/params/repo_query_param.dart';
import 'package:result_monad/result_monad.dart';

abstract class HomeRepository {
  ///MARK:- REMOTE
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getRemoteGithubPaginatedRepos(RepoQueryParam param);

  ///MARK:- LOCAL
  Future<Result<PagedList<RepoEntity>, ErrorModel>>
      getLocalGithubPaginatedRepos(String pageNumber);

  Future<void> insertRepositoryTableEntryList(
      List<RepoEntity> list, String page_number);
}
