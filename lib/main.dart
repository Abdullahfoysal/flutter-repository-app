import 'package:flutter/material.dart';
import 'package:flutter_repository_app/app/app.bottomsheets.dart';
import 'package:flutter_repository_app/app/app.dialogs.dart';
import 'package:flutter_repository_app/app/app.locator.dart';
import 'package:flutter_repository_app/app/app.router.dart';
import 'package:stacked_services/stacked_services.dart';

import 'app/di/app_di_module.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  await setupLocator();
  setupDialogUi();
  setupBottomSheetUi();
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: Routes.startupView,
      debugShowCheckedModeBanner: false,
      onGenerateRoute: StackedRouter().onGenerateRoute,
      navigatorKey: StackedService.navigatorKey,
      navigatorObservers: [
        StackedService.routeObserver,
      ],
    );
  }
}
