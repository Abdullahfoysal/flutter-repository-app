const isProduction = false;
final devEnvironment = {
  "BASE_URL": "https://api.github.com",
  "BASE_PHOTO_URL": "",
  "TEAM_PHOTO_URL": "",
  "GOOGLE_MAP_API_KEY": "",
  "IMAGE_URL": "",
  "BASE_WEB_CLIENT_URL": "",
  "trustSelfSigned": false
};

final prodEnvironment = {
  "BASE_URL": "https://api.github.com",
  "BASE_PHOTO_URL": "",
  "TEAM_PHOTO_URL": "",
  "GOOGLE_MAP_API_KEY": "",
  "IMAGE_URL": "",
  "BASE_WEB_CLIENT_URL": "",
  "trustSelfSigned": false
};

final environment = isProduction ? prodEnvironment : devEnvironment;
