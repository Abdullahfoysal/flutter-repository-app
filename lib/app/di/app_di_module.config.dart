// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:pretty_dio_logger/pretty_dio_logger.dart' as _i7;

import '../../ui/views/home/data/repository_imp/home_repository_imp.dart'
    as _i14;
import '../../ui/views/home/data/sources/local/home_local_repository.dart'
    as _i4;
import '../../ui/views/home/data/sources/local/home_local_repository_imp.dart'
    as _i5;
import '../../ui/views/home/data/sources/remote/api_services/github_api.dart'
    as _i10;
import '../../ui/views/home/data/sources/remote/remote_services/home_remote_repository.dart'
    as _i11;
import '../../ui/views/home/data/sources/remote/remote_services/home_remote_repository_imp.dart'
    as _i12;
import '../../ui/views/home/domain/repository/home_repository.dart' as _i13;
import '../../ui/views/home/domain/usecases/local/get_local_paginated_repos_usecase.dart'
    as _i16;
import '../../ui/views/home/domain/usecases/local/save_local_paginated_repos_usecase.dart'
    as _i15;
import '../../ui/views/home/domain/usecases/remote/get_paginated_repos_from_server_usecase.dart'
    as _i17;
import '../../ui/views/home/presentation/viewModels/home_viewmodel.dart'
    as _i18;
import '../../ui/views/repository_details/repository_details_viewmodel.dart'
    as _i8;
import '../shared/local/app_database.dart' as _i6;
import '../shared/network/network_module.dart' as _i19;
import '../shared/preference/UserPrefManager.dart' as _i9;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final networkModule = _$NetworkModule();
    gh.lazySingleton<_i3.Dio>(() => networkModule.dio);
    gh.factory<_i4.HomeLocalRepository>(
        () => _i5.HomeLocalRepositoryImp(gh<_i6.AppDatabase>()));
    gh.lazySingleton<Map<String, String>>(() => networkModule.headers);
    gh.lazySingleton<_i7.PrettyDioLogger>(
        () => networkModule.dioLoggingInterceptor);
    gh.factory<_i8.RepositoryDetailsViewModel>(
        () => _i8.RepositoryDetailsViewModel());
    gh.factory<String>(
      () => networkModule.baseURL,
      instanceName: 'baseURL',
    );
    gh.factory<_i9.UserPrefManager>(() => _i9.UserPrefManager());
    gh.factory<_i10.GithubApi>(() => _i10.GithubApi(
          gh<_i3.Dio>(),
          baseUrl: gh<String>(instanceName: 'baseURL'),
        ));
    gh.factory<_i11.HomeRemoteRepository>(
        () => _i12.HomeRemoteRepositoryImp(gh<_i10.GithubApi>()));
    gh.factory<_i13.HomeRepository>(() => _i14.HomeRepositoryImp(
          gh<_i11.HomeRemoteRepository>(),
          gh<_i4.HomeLocalRepository>(),
        ));
    gh.factory<_i15.SaveLocalPaginatedReposUsecase>(
        () => _i15.SaveLocalPaginatedReposUsecase(gh<_i13.HomeRepository>()));
    gh.factory<_i16.GetLocalPaginatedReposUsecase>(
        () => _i16.GetLocalPaginatedReposUsecase(gh<_i13.HomeRepository>()));
    gh.factory<_i17.GetPaginatedReposFromServerUsecase>(() =>
        _i17.GetPaginatedReposFromServerUsecase(gh<_i13.HomeRepository>()));
    gh.factory<_i18.HomeViewModel>(() => _i18.HomeViewModel(
          gh<_i17.GetPaginatedReposFromServerUsecase>(),
          gh<_i16.GetLocalPaginatedReposUsecase>(),
          gh<_i15.SaveLocalPaginatedReposUsecase>(),
        ));
    return this;
  }
}

class _$NetworkModule extends _i19.NetworkModule {}
