import 'package:floor/floor.dart';
import 'package:flutter_repository_app/app/shared/local/database_constants.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import '../shared/local/app_database.dart';
import '../shared/local/database_migrate/database_migrate.dart';
import 'app_di_module.config.dart';

final getIt = GetIt.instance;
final migration1to2 = Migration(1, 2, migrateDatabase1to2);

@InjectableInit(
  initializerName: 'init', // default
  preferRelativeImports: true, // default
  asExtension: true, // default
)
Future configureDependencies() async {
  getIt.init();
  await databaseInject();
}

Future databaseInject() async {
  final database = await $FloorAppDatabase
      .databaseBuilder(DatabaseConstants.databaseName)
      .addMigrations([migration1to2]).build();
  getIt.registerSingleton<AppDatabase>(database);
}
