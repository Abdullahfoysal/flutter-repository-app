class PagedList<T> {
  PagedList(this.list, this.totalCount);

  final List<T> list;
  final int totalCount;
}
