import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@injectable
class UserPrefManager {
  final String _accessTokenKey = 'access_token';
  static final String _lastRefreshedTime = 'last_refresh_time';
  static final String _latestTotalRepoCount = 'latest_repo_count';
  static final String _isSortByStartCount = '_isSortByStartCount';

  ///MARK:- TOKEN
  saveAccessToken(String accessToken) async {
    var preference = await SharedPreferences.getInstance();
    await preference.setString(_accessTokenKey, accessToken);
  }

  Future<String> getAccessToken() async {
    var preference = await SharedPreferences.getInstance();
    return preference.getString(_accessTokenKey) ?? "";
  }

  ///MARK:- LAST REFRESH TIME
  static saveLastRefreshTime(DateTime dateTime) async {
    var preference = await SharedPreferences.getInstance();
    await preference.setString(_lastRefreshedTime, dateTime.toString());
  }

  static Future<String?> getLastRefreshTime() async {
    var preference = await SharedPreferences.getInstance();
    return preference.getString(_lastRefreshedTime);
  }

  ///MARK:- TOTAL Repository COUNT ON LOCAL
  static saveLatestTotalRepoCount(int count) async {
    var preference = await SharedPreferences.getInstance();
    await preference.setInt(_latestTotalRepoCount, count);
  }

  static Future<int> getLatestRepoCount() async {
    var preference = await SharedPreferences.getInstance();
    return preference.getInt(_latestTotalRepoCount) ?? 0;
  }

  ///MARK:- sort by star count
  static Future<bool> saveIsSortByStarCount(bool isSortByStar) async {
    var preference = await SharedPreferences.getInstance();
    return await preference.setBool(_isSortByStartCount, isSortByStar);
  }

  static Future<bool> getIsSortByStarCount() async {
    var preference = await SharedPreferences.getInstance();
    return preference.getBool(_isSortByStartCount) ?? false;
  }

  Future<bool> clearSharedPrefData() async {
    var preference = await SharedPreferences.getInstance();
    return await preference.clear();
  }
}
