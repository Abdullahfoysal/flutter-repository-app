import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:stacked_services/stacked_services.dart';

import '../../app.dialogs.dart';
import '../../app.locator.dart';
import '../../di/app_di_module.dart';
import '../preference/UserPrefManager.dart';

class AuthInterceptor extends Interceptor {
  var pref = UserPrefManager();
  final NavigationService _navigationService = getIt<NavigationService>();
  final _dialogService = locator<DialogService>();

  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final token = await pref.getAccessToken();
    if (token != "") {
      options.headers.addAll({"Authorization": "Bearer $token"});
    }
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    // TODO: implement onResponse
    super.onResponse(response, handler);
  }

  @override
  Future<void> onError(
      DioException err, ErrorInterceptorHandler handler) async {
    super.onError(err, handler);
    if (err.type == DioExceptionType.connectionTimeout ||
        err.type == DioExceptionType.receiveTimeout) {
      //showErrorAlert("Connection Timeout", _navigationService.getCurrentContext!);
    } else {
      if (err.response?.realUri.path == '/logs') {
      } else {
        _handleError(err.response?.statusCode ?? 0);
      }
    }
  }

  _handleError(int statusCode) async {
    switch (statusCode) {
      case 400:
        // showErrorAlert("Bad request", _navigationService.getCurrentContext!);

        showDialog("Opps!!!", "Bad request");
        break;
      case 403:
        // showErrorAlert("Not Found", _navigationService.getCurrentContext!);

        showDialog("Opps!!!", "Forbidden 403");
        break;
      case 404:
        // showErrorAlert("Not Found", _navigationService.getCurrentContext!);

        showDialog("Opps!!!", "Not Found 404");
        break;
      case 500:
        /*  showErrorAlert(
            "Internal server error", _navigationService.getCurrentContext!);*/

        showDialog("Opps!!!", "Not Found 404");
        break;
      case 401:
        /* if (SessionManager.instance.isLoggedIn) {
          SessionManager.instance.isLoggedIn = false;
          getIt<SettingScreenViewModel>().onSignOutClick();
        }*/

        showDialog("Opps!!!", "No Access 401");
        break;
      default:
        var hasInternet = await checkInternetStatus();
        var message = hasInternet
            ? "Something went wrong!"
            : "Check your internet connection!";

        showDialog("Request status", message);
        // showErrorAlert(message, _navigationService.getCurrentContext!);

        break;
    }
  }

  void showDialog(String label, String message) {
    _dialogService.showCustomDialog(
      variant: DialogType.infoAlert,
      title: label,
      description: message,
    );
  }
}

Future<bool> checkInternetStatus() async {
  try {
    final url = Uri.https('google.com');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
}
