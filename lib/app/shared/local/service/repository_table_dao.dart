import 'package:floor/floor.dart';
import 'package:flutter_repository_app/app/shared/local/database_constants.dart';

import '../table_entity/repository_table/repository_entity_table.dart';

@dao
abstract class RepositoryTableDao {
  @Query(
      'SELECT * FROM ${DatabaseConstants.repositoryTableName}  WHERE ${DatabaseConstants.repositoryTableName}.page_number = :pageNumber')
  Future<List<RepositoryEntityTable>> getPaginatedGithubRepos(
      String pageNumber);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertRepositoryEntry(
      RepositoryEntityTable repositoryEntityTable);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertRepositoryEntryList(List<RepositoryEntityTable> list);

  @delete
  Future<void> deleteRepositoryEntry(
      RepositoryEntityTable repositoryEntityTable);

  @Query('DELETE FROM ${DatabaseConstants.repositoryTableName}')
  Future<void> deleteAll();
}
