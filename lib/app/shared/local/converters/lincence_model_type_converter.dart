import 'dart:convert';

import 'package:floor/floor.dart';

import '../../../../ui/views/home/data/models/licence_model.dart';

class LicenceModelTypeConverter extends TypeConverter<LicenseModel?, String> {
  @override
  LicenseModel? decode(String? databaseValue) {
    if (databaseValue == null || databaseValue == "null") return null;
    return LicenseModel.fromJson(json.decode(databaseValue));
  }

  @override
  String encode(LicenseModel? value) {
    if (value == null) return "";
    return json.encode(value?.toJson());
  }
}
