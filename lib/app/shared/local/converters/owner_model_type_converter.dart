import 'dart:convert';

import 'package:floor/floor.dart';
import 'package:flutter_repository_app/ui/views/home/data/models/owner_model.dart';

class OwnerModelTypeConverter extends TypeConverter<OwnerModel?, String> {
  @override
  OwnerModel? decode(String databaseValue) {
    if (databaseValue == null || databaseValue == "null") return null;
    return OwnerModel.fromJson(json.decode(databaseValue));
  }

  @override
  String encode(OwnerModel? value) {
    if (value == null) return "";
    return json.encode(value?.toJson());
  }
}
