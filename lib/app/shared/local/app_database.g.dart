// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  RepositoryTableDao? _repositoryTableDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `repository_table` (`id` INTEGER NOT NULL, `page_number` TEXT, `node_id` TEXT, `name` TEXT, `full_name` TEXT, `private` INTEGER, `owner` TEXT, `html_url` TEXT, `description` TEXT, `fork` INTEGER, `url` TEXT, `forks_url` TEXT, `keys_url` TEXT, `collaborators_url` TEXT, `teams_url` TEXT, `hooks_url` TEXT, `issue_events_url` TEXT, `events_url` TEXT, `assignees_url` TEXT, `branches_url` TEXT, `tags_url` TEXT, `blobs_url` TEXT, `git_tags_url` TEXT, `git_refs_url` TEXT, `trees_url` TEXT, `statuses_url` TEXT, `languages_url` TEXT, `stargazers_url` TEXT, `contributors_url` TEXT, `subscribers_url` TEXT, `subscription_url` TEXT, `commits_url` TEXT, `git_commits_url` TEXT, `comments_url` TEXT, `issue_comment_url` TEXT, `contents_url` TEXT, `compare_url` TEXT, `merges_url` TEXT, `archive_url` TEXT, `downloads_url` TEXT, `issues_url` TEXT, `pulls_url` TEXT, `milestones_url` TEXT, `notifications_url` TEXT, `labels_url` TEXT, `releases_url` TEXT, `deployments_url` TEXT, `created_at` TEXT, `updated_at` TEXT, `pushed_at` TEXT, `git_url` TEXT, `ssh_url` TEXT, `clone_url` TEXT, `svn_url` TEXT, `homepage` TEXT, `size` INTEGER, `stargazers_count` INTEGER, `watchers_count` INTEGER, `language` TEXT, `has_issues` INTEGER, `has_projects` INTEGER, `has_downloads` INTEGER, `has_wiki` INTEGER, `has_pages` INTEGER, `has_discussions` INTEGER, `forks_count` INTEGER, `mirror_url` TEXT, `archived` INTEGER, `disabled` INTEGER, `open_issues_count` INTEGER, `allow_forking` INTEGER, `web_commit_signoff_required` INTEGER, `visibility` TEXT, `forks` INTEGER, `open_issues` INTEGER, `watchers` INTEGER, `default_branch` TEXT, `score` REAL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  RepositoryTableDao get repositoryTableDao {
    return _repositoryTableDaoInstance ??=
        _$RepositoryTableDao(database, changeListener);
  }
}

class _$RepositoryTableDao extends RepositoryTableDao {
  _$RepositoryTableDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _repositoryEntityTableInsertionAdapter = InsertionAdapter(
            database,
            'repository_table',
            (RepositoryEntityTable item) => <String, Object?>{
                  'id': item.id,
                  'page_number': item.page_number,
                  'node_id': item.node_id,
                  'name': item.name,
                  'full_name': item.full_name,
                  'private':
                      item.private == null ? null : (item.private! ? 1 : 0),
                  'owner': _ownerModelTypeConverter.encode(item.owner),
                  'html_url': item.html_url,
                  'description': item.description,
                  'fork': item.fork == null ? null : (item.fork! ? 1 : 0),
                  'url': item.url,
                  'forks_url': item.forks_url,
                  'keys_url': item.keys_url,
                  'collaborators_url': item.collaborators_url,
                  'teams_url': item.teams_url,
                  'hooks_url': item.hooks_url,
                  'issue_events_url': item.issue_events_url,
                  'events_url': item.events_url,
                  'assignees_url': item.assignees_url,
                  'branches_url': item.branches_url,
                  'tags_url': item.tags_url,
                  'blobs_url': item.blobs_url,
                  'git_tags_url': item.git_tags_url,
                  'git_refs_url': item.git_refs_url,
                  'trees_url': item.trees_url,
                  'statuses_url': item.statuses_url,
                  'languages_url': item.languages_url,
                  'stargazers_url': item.stargazers_url,
                  'contributors_url': item.contributors_url,
                  'subscribers_url': item.subscribers_url,
                  'subscription_url': item.subscription_url,
                  'commits_url': item.commits_url,
                  'git_commits_url': item.git_commits_url,
                  'comments_url': item.comments_url,
                  'issue_comment_url': item.issue_comment_url,
                  'contents_url': item.contents_url,
                  'compare_url': item.compare_url,
                  'merges_url': item.merges_url,
                  'archive_url': item.archive_url,
                  'downloads_url': item.downloads_url,
                  'issues_url': item.issues_url,
                  'pulls_url': item.pulls_url,
                  'milestones_url': item.milestones_url,
                  'notifications_url': item.notifications_url,
                  'labels_url': item.labels_url,
                  'releases_url': item.releases_url,
                  'deployments_url': item.deployments_url,
                  'created_at': item.created_at,
                  'updated_at': item.updated_at,
                  'pushed_at': item.pushed_at,
                  'git_url': item.git_url,
                  'ssh_url': item.ssh_url,
                  'clone_url': item.clone_url,
                  'svn_url': item.svn_url,
                  'homepage': item.homepage,
                  'size': item.size,
                  'stargazers_count': item.stargazers_count,
                  'watchers_count': item.watchers_count,
                  'language': item.language,
                  'has_issues': item.has_issues == null
                      ? null
                      : (item.has_issues! ? 1 : 0),
                  'has_projects': item.has_projects == null
                      ? null
                      : (item.has_projects! ? 1 : 0),
                  'has_downloads': item.has_downloads == null
                      ? null
                      : (item.has_downloads! ? 1 : 0),
                  'has_wiki':
                      item.has_wiki == null ? null : (item.has_wiki! ? 1 : 0),
                  'has_pages':
                      item.has_pages == null ? null : (item.has_pages! ? 1 : 0),
                  'has_discussions': item.has_discussions == null
                      ? null
                      : (item.has_discussions! ? 1 : 0),
                  'forks_count': item.forks_count,
                  'mirror_url': item.mirror_url,
                  'archived':
                      item.archived == null ? null : (item.archived! ? 1 : 0),
                  'disabled':
                      item.disabled == null ? null : (item.disabled! ? 1 : 0),
                  'open_issues_count': item.open_issues_count,
                  'allow_forking': item.allow_forking == null
                      ? null
                      : (item.allow_forking! ? 1 : 0),
                  'web_commit_signoff_required':
                      item.web_commit_signoff_required == null
                          ? null
                          : (item.web_commit_signoff_required! ? 1 : 0),
                  'visibility': item.visibility,
                  'forks': item.forks,
                  'open_issues': item.open_issues,
                  'watchers': item.watchers,
                  'default_branch': item.default_branch,
                  'score': item.score
                }),
        _repositoryEntityTableDeletionAdapter = DeletionAdapter(
            database,
            'repository_table',
            ['id'],
            (RepositoryEntityTable item) => <String, Object?>{
                  'id': item.id,
                  'page_number': item.page_number,
                  'node_id': item.node_id,
                  'name': item.name,
                  'full_name': item.full_name,
                  'private':
                      item.private == null ? null : (item.private! ? 1 : 0),
                  'owner': _ownerModelTypeConverter.encode(item.owner),
                  'html_url': item.html_url,
                  'description': item.description,
                  'fork': item.fork == null ? null : (item.fork! ? 1 : 0),
                  'url': item.url,
                  'forks_url': item.forks_url,
                  'keys_url': item.keys_url,
                  'collaborators_url': item.collaborators_url,
                  'teams_url': item.teams_url,
                  'hooks_url': item.hooks_url,
                  'issue_events_url': item.issue_events_url,
                  'events_url': item.events_url,
                  'assignees_url': item.assignees_url,
                  'branches_url': item.branches_url,
                  'tags_url': item.tags_url,
                  'blobs_url': item.blobs_url,
                  'git_tags_url': item.git_tags_url,
                  'git_refs_url': item.git_refs_url,
                  'trees_url': item.trees_url,
                  'statuses_url': item.statuses_url,
                  'languages_url': item.languages_url,
                  'stargazers_url': item.stargazers_url,
                  'contributors_url': item.contributors_url,
                  'subscribers_url': item.subscribers_url,
                  'subscription_url': item.subscription_url,
                  'commits_url': item.commits_url,
                  'git_commits_url': item.git_commits_url,
                  'comments_url': item.comments_url,
                  'issue_comment_url': item.issue_comment_url,
                  'contents_url': item.contents_url,
                  'compare_url': item.compare_url,
                  'merges_url': item.merges_url,
                  'archive_url': item.archive_url,
                  'downloads_url': item.downloads_url,
                  'issues_url': item.issues_url,
                  'pulls_url': item.pulls_url,
                  'milestones_url': item.milestones_url,
                  'notifications_url': item.notifications_url,
                  'labels_url': item.labels_url,
                  'releases_url': item.releases_url,
                  'deployments_url': item.deployments_url,
                  'created_at': item.created_at,
                  'updated_at': item.updated_at,
                  'pushed_at': item.pushed_at,
                  'git_url': item.git_url,
                  'ssh_url': item.ssh_url,
                  'clone_url': item.clone_url,
                  'svn_url': item.svn_url,
                  'homepage': item.homepage,
                  'size': item.size,
                  'stargazers_count': item.stargazers_count,
                  'watchers_count': item.watchers_count,
                  'language': item.language,
                  'has_issues': item.has_issues == null
                      ? null
                      : (item.has_issues! ? 1 : 0),
                  'has_projects': item.has_projects == null
                      ? null
                      : (item.has_projects! ? 1 : 0),
                  'has_downloads': item.has_downloads == null
                      ? null
                      : (item.has_downloads! ? 1 : 0),
                  'has_wiki':
                      item.has_wiki == null ? null : (item.has_wiki! ? 1 : 0),
                  'has_pages':
                      item.has_pages == null ? null : (item.has_pages! ? 1 : 0),
                  'has_discussions': item.has_discussions == null
                      ? null
                      : (item.has_discussions! ? 1 : 0),
                  'forks_count': item.forks_count,
                  'mirror_url': item.mirror_url,
                  'archived':
                      item.archived == null ? null : (item.archived! ? 1 : 0),
                  'disabled':
                      item.disabled == null ? null : (item.disabled! ? 1 : 0),
                  'open_issues_count': item.open_issues_count,
                  'allow_forking': item.allow_forking == null
                      ? null
                      : (item.allow_forking! ? 1 : 0),
                  'web_commit_signoff_required':
                      item.web_commit_signoff_required == null
                          ? null
                          : (item.web_commit_signoff_required! ? 1 : 0),
                  'visibility': item.visibility,
                  'forks': item.forks,
                  'open_issues': item.open_issues,
                  'watchers': item.watchers,
                  'default_branch': item.default_branch,
                  'score': item.score
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<RepositoryEntityTable>
      _repositoryEntityTableInsertionAdapter;

  final DeletionAdapter<RepositoryEntityTable>
      _repositoryEntityTableDeletionAdapter;

  @override
  Future<List<RepositoryEntityTable>> getPaginatedGithubRepos(
      String pageNumber) async {
    return _queryAdapter.queryList(
        'SELECT * FROM repository_table  WHERE repository_table.page_number = ?1',
        mapper: (Map<String, Object?> row) => RepositoryEntityTable(id: row['id'] as int, page_number: row['page_number'] as String?, node_id: row['node_id'] as String?, name: row['name'] as String?, full_name: row['full_name'] as String?, private: row['private'] == null ? null : (row['private'] as int) != 0, owner: _ownerModelTypeConverter.decode(row['owner'] as String), html_url: row['html_url'] as String?, description: row['description'] as String?, fork: row['fork'] == null ? null : (row['fork'] as int) != 0, url: row['url'] as String?, forks_url: row['forks_url'] as String?, keys_url: row['keys_url'] as String?, collaborators_url: row['collaborators_url'] as String?, teams_url: row['teams_url'] as String?, hooks_url: row['hooks_url'] as String?, issue_events_url: row['issue_events_url'] as String?, events_url: row['events_url'] as String?, assignees_url: row['assignees_url'] as String?, branches_url: row['branches_url'] as String?, tags_url: row['tags_url'] as String?, blobs_url: row['blobs_url'] as String?, git_tags_url: row['git_tags_url'] as String?, git_refs_url: row['git_refs_url'] as String?, trees_url: row['trees_url'] as String?, statuses_url: row['statuses_url'] as String?, languages_url: row['languages_url'] as String?, stargazers_url: row['stargazers_url'] as String?, contributors_url: row['contributors_url'] as String?, subscribers_url: row['subscribers_url'] as String?, subscription_url: row['subscription_url'] as String?, commits_url: row['commits_url'] as String?, git_commits_url: row['git_commits_url'] as String?, comments_url: row['comments_url'] as String?, issue_comment_url: row['issue_comment_url'] as String?, contents_url: row['contents_url'] as String?, compare_url: row['compare_url'] as String?, merges_url: row['merges_url'] as String?, archive_url: row['archive_url'] as String?, downloads_url: row['downloads_url'] as String?, issues_url: row['issues_url'] as String?, pulls_url: row['pulls_url'] as String?, milestones_url: row['milestones_url'] as String?, notifications_url: row['notifications_url'] as String?, labels_url: row['labels_url'] as String?, releases_url: row['releases_url'] as String?, deployments_url: row['deployments_url'] as String?, created_at: row['created_at'] as String?, updated_at: row['updated_at'] as String?, pushed_at: row['pushed_at'] as String?, git_url: row['git_url'] as String?, ssh_url: row['ssh_url'] as String?, clone_url: row['clone_url'] as String?, svn_url: row['svn_url'] as String?, homepage: row['homepage'] as String?, size: row['size'] as int?, stargazers_count: row['stargazers_count'] as int?, watchers_count: row['watchers_count'] as int?, language: row['language'] as String?, has_issues: row['has_issues'] == null ? null : (row['has_issues'] as int) != 0, has_projects: row['has_projects'] == null ? null : (row['has_projects'] as int) != 0, has_downloads: row['has_downloads'] == null ? null : (row['has_downloads'] as int) != 0, has_wiki: row['has_wiki'] == null ? null : (row['has_wiki'] as int) != 0, has_pages: row['has_pages'] == null ? null : (row['has_pages'] as int) != 0, has_discussions: row['has_discussions'] == null ? null : (row['has_discussions'] as int) != 0, forks_count: row['forks_count'] as int?, mirror_url: row['mirror_url'] as String?, archived: row['archived'] == null ? null : (row['archived'] as int) != 0, disabled: row['disabled'] == null ? null : (row['disabled'] as int) != 0, open_issues_count: row['open_issues_count'] as int?, allow_forking: row['allow_forking'] == null ? null : (row['allow_forking'] as int) != 0, web_commit_signoff_required: row['web_commit_signoff_required'] == null ? null : (row['web_commit_signoff_required'] as int) != 0, visibility: row['visibility'] as String?, forks: row['forks'] as int?, open_issues: row['open_issues'] as int?, watchers: row['watchers'] as int?, default_branch: row['default_branch'] as String?, score: row['score'] as double?),
        arguments: [pageNumber]);
  }

  @override
  Future<void> deleteAll() async {
    await _queryAdapter.queryNoReturn('DELETE FROM repository_table');
  }

  @override
  Future<void> insertRepositoryEntry(
      RepositoryEntityTable repositoryEntityTable) async {
    await _repositoryEntityTableInsertionAdapter.insert(
        repositoryEntityTable, OnConflictStrategy.replace);
  }

  @override
  Future<void> insertRepositoryEntryList(
      List<RepositoryEntityTable> list) async {
    await _repositoryEntityTableInsertionAdapter.insertList(
        list, OnConflictStrategy.replace);
  }

  @override
  Future<void> deleteRepositoryEntry(
      RepositoryEntityTable repositoryEntityTable) async {
    await _repositoryEntityTableDeletionAdapter.delete(repositoryEntityTable);
  }
}

// ignore_for_file: unused_element
final _ownerModelTypeConverter = OwnerModelTypeConverter();
final _licenceModelTypeConverter = LicenceModelTypeConverter();
