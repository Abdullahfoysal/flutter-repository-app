// ignore_for_file: duplicate_import, unused_import

import 'dart:async';

import 'package:floor/floor.dart';
import 'package:flutter_repository_app/app/shared/local/converters/lincence_model_type_converter.dart';
import 'package:flutter_repository_app/app/shared/local/service/repository_table_dao.dart';
import 'package:flutter_repository_app/app/shared/local/table_entity/repository_table/repository_entity_table.dart';
import 'package:flutter_repository_app/ui/views/home/data/models/licence_model.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'converters/owner_model_type_converter.dart';

part 'app_database.g.dart';

@TypeConverters([OwnerModelTypeConverter, LicenceModelTypeConverter])
@Database(version: 1, entities: [
  RepositoryEntityTable,
])
abstract class AppDatabase extends FloorDatabase {
  RepositoryTableDao get repositoryTableDao;
}
