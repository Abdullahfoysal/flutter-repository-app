import 'package:flutter_repository_app/app/shared/local/database_constants.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

Future<void> migrateDatabase1to2(sqflite.Database database) async {
  await database.execute(
      'ALTER TABLE ${DatabaseConstants.repositoryTableName} ADD COLUMN saved_time TEXT');
}
