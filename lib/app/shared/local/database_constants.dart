final class DatabaseConstants {
  static const String databaseName = "flutter_repo_local@db";

  ///MARK: -Table Name
  static const String repositoryTableName = "repository_table";
}
