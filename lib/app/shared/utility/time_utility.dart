import 'package:intl/intl.dart';

final class TimeUtility {
  static String edMYHMformatDate(String? dateString) {
    if (dateString == null) return "";
    DateTime dateTime = DateTime.parse(dateString);
    return DateFormat('MM-dd-yyyy H:ss').format(dateTime);
  }
}
