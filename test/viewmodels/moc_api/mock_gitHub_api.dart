import 'package:flutter_repository_app/ui/views/home/data/models/flutter_repos_response_model.dart';
import 'package:flutter_repository_app/ui/views/home/data/models/item_model.dart';
import 'package:flutter_repository_app/ui/views/home/data/sources/remote/api_services/github_api.dart';

class MockGitHubApi implements GithubApi {
  @override
  Future<FlutterReposResponseModel> getGithubPaginatedRepos(
      String featureName, String pageSize, String pageNumber) {
    var response = FlutterReposResponseModel();
    response.total_count = 100;
    response.items = [
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
      ItemModel(),
    ];

    return Future.value(response);
  }
}
