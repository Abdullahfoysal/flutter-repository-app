import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_repository_app/app/core/models/page_list.dart';
import 'package:flutter_repository_app/ui/common/app_constants.dart';
import 'package:flutter_repository_app/ui/views/home/domain/entities/repo_entity.dart';
import 'package:flutter_repository_app/ui/views/home/domain/usecases/remote/get_paginated_repos_from_server_usecase.dart';
import 'package:flutter_repository_app/ui/views/home/params/repo_query_param.dart';
import 'package:flutter_repository_app/ui/views/home/params/repo_save_to_local_param.dart';
import 'package:flutter_repository_app/ui/views/home/presentation/viewModels/home_viewmodel.dart';
import 'package:infinite_scroll_pagination/src/core/paging_controller.dart';

class MockHomeViewModel implements HomeViewModel {
  final GetPaginatedReposFromServerUsecase _getPaginatedReposFromServerUsecase;
  MockHomeViewModel(this._getPaginatedReposFromServerUsecase);
  @override
  late bool disposed;

  @override
  void addListener(VoidCallback listener) {
    // TODO: implement addListener
  }

  @override
  // TODO: implement anyObjectsBusy
  bool get anyObjectsBusy => throw UnimplementedError();

  @override
  bool busy(Object? object) {
    // TODO: implement busy
    throw UnimplementedError();
  }

  @override
  void clearErrors() {
    // TODO: implement clearErrors
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  error(Object object) {
    // TODO: implement error
    throw UnimplementedError();
  }

  @override
  Future<PagedList<RepoEntity>> fetchPageFromLocal(int page) {
    // TODO: implement fetchPageFromLocal
    throw UnimplementedError();
  }

  @override
  Future<PagedList<RepoEntity>> fetchPageFromRemote(int page) {
    RepoQueryParam repoQueryParam = RepoQueryParam(
        featureName: "flutter", pageSize: "$pageSize", pageNumber: "$page");
    return _getPaginatedReposFromServerUsecase
        .invoke(repoQueryParam)
        .then((value) => value.value);
  }

  @override
  // TODO: implement firstPageProgressIndicator
  Widget get firstPageProgressIndicator => throw UnimplementedError();

  @override
  // TODO: implement hasError
  bool get hasError => throw UnimplementedError();

  @override
  bool hasErrorForKey(Object key) {
    // TODO: implement hasErrorForKey
    throw UnimplementedError();
  }

  @override
  // TODO: implement hasListeners
  bool get hasListeners => throw UnimplementedError();

  @override
  void incrementCounter() {
    // TODO: implement incrementCounter
  }

  @override
  // TODO: implement initialised
  bool get initialised => throw UnimplementedError();

  @override
  // TODO: implement isBusy
  bool get isBusy => throw UnimplementedError();

  @override
  // TODO: implement modelError
  get modelError => throw UnimplementedError();

  @override
  // TODO: implement newPageProgressIndicator
  Widget get newPageProgressIndicator => throw UnimplementedError();

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void onFutureError(error, Object? key) {
    // TODO: implement onFutureError
  }

  @override
  // TODO: implement onModelReadyCalled
  bool get onModelReadyCalled => throw UnimplementedError();

  @override
  // TODO: implement pagingController
  PagingController<int, RepoEntity> get pagingController =>
      throw UnimplementedError();

  @override
  void rebuildUi() {
    // TODO: implement rebuildUi
  }

  @override
  void removeListener(VoidCallback listener) {
    // TODO: implement removeListener
  }

  @override
  Future<T> runBusyFuture<T>(Future<T> busyFuture,
      {Object? busyObject, bool throwException = false}) {
    // TODO: implement runBusyFuture
    throw UnimplementedError();
  }

  @override
  Future<T> runErrorFuture<T>(Future<T> future,
      {Object? key, bool throwException = false}) {
    // TODO: implement runErrorFuture
    throw UnimplementedError();
  }

  @override
  void savePaginatedReposToLocal(
      RepoSaveToLocalParam repoSaveToLocalParam, int totalCount) {
    // TODO: implement savePaginatedReposToLocal
  }

  @override
  void setBusy(bool value) {
    // TODO: implement setBusy
  }

  @override
  void setBusyForObject(Object? object, bool value) {
    // TODO: implement setBusyForObject
  }

  @override
  void setError(error) {
    // TODO: implement setError
  }

  @override
  void setErrorForModelOrObject(value, {Object? key}) {
    // TODO: implement setErrorForModelOrObject
  }

  @override
  void setErrorForObject(Object object, value) {
    // TODO: implement setErrorForObject
  }

  @override
  void setInitialised(bool value) {
    // TODO: implement setInitialised
  }

  @override
  void setOnModelReadyCalled(bool value) {
    // TODO: implement setOnModelReadyCalled
  }

  @override
  void showBottomSheet() {
    // TODO: implement showBottomSheet
  }

  @override
  singleRepoItemView(repoEntity) {
    // TODO: implement singleRepoItemView
    throw UnimplementedError();
  }

  @override
  T skeletonData<T>(
      {required T? realData, required T busyData, Object? busyKey}) {
    // TODO: implement skeletonData
    throw UnimplementedError();
  }

  @override
  void sortByStarCount() {
    // TODO: implement sortByStarCount
  }
}
