import 'package:flutter_repository_app/ui/views/home/data/repository_imp/home_repository_imp.dart';
import 'package:flutter_repository_app/ui/views/home/data/sources/local/home_local_repository_imp.dart';
import 'package:flutter_repository_app/ui/views/home/data/sources/remote/remote_services/home_remote_repository_imp.dart';
import 'package:flutter_repository_app/ui/views/home/domain/usecases/local/get_local_paginated_repos_usecase.dart';
import 'package:flutter_repository_app/ui/views/home/domain/usecases/local/save_local_paginated_repos_usecase.dart';
import 'package:flutter_repository_app/ui/views/home/domain/usecases/remote/get_paginated_repos_from_server_usecase.dart';
import 'package:flutter_repository_app/ui/views/home/presentation/viewModels/home_viewmodel.dart';
import 'package:flutter_test/flutter_test.dart';

import 'database/moc_database.dart';
import 'moc_api/mock_gitHub_api.dart';
import 'moc_viewmodel/mock_home_view_model.dart';

void main() async {
  late HomeViewModel viewModel;

  group('HomeViewmodelTest -', () {
    setUp(() async {
      final database = MocDatabase();

      var _githubApi = MockGitHubApi();

      var _homeRemoteRepository = HomeRemoteRepositoryImp(_githubApi);

      var _homeLocalRepository = HomeLocalRepositoryImp(database);
      var _homeRepository =
          HomeRepositoryImp(_homeRemoteRepository, _homeLocalRepository);
      var getPaginatedReposFromServerUsecase =
          GetPaginatedReposFromServerUsecase(_homeRepository);
      var getLocalPaginatedReposUsecase =
          GetLocalPaginatedReposUsecase(_homeRepository);
      var saveLocalPaginatedReposUsecase =
          SaveLocalPaginatedReposUsecase(_homeRepository);
      viewModel = MockHomeViewModel(getPaginatedReposFromServerUsecase);
    });
    tearDown(() {
      print("Ends everything");
    });

    group('first repository page from server -', () {
      test('Github Remote api should return 10 repository for page 1',
          () async {
        var list = await viewModel.fetchPageFromRemote(1);
        expect(list.list.length, 10);
      });

      test('Github Remote api should return 10 repository for page 2',
          () async {
        var list = await viewModel.fetchPageFromRemote(2);
        expect(list.list.length, 10);
      });
    });
  });
}
