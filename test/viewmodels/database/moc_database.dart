import 'dart:async';

import 'package:flutter_repository_app/app/shared/local/app_database.dart';
import 'package:flutter_repository_app/app/shared/local/service/repository_table_dao.dart';
import 'package:sqflite_common/sqlite_api.dart';

class MocDatabase implements AppDatabase {
  @override
  late StreamController<String> changeListener;

  @override
  late DatabaseExecutor database;

  @override
  Future<void> close() {
    // TODO: implement close
    throw UnimplementedError();
  }

  @override
  // TODO: implement repositoryTableDao
  RepositoryTableDao get repositoryTableDao => throw UnimplementedError();
}
