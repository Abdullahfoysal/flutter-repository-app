# Flutter's Repository App
#fvm flutter --version
- Flutter 3.16.9 • channel stable 
- Tools • Dart 3.2.6 • DevTools 2.28.5

<p align="center">
  <img src="ss/1.png" alt="Preview" width="200"  />
  <img src="ss/2.png" alt="List" width="200" />
  <img src="ss/3.png" alt="Details" width="200" />
  <img src="ss/4.png" alt="Details" width="200" />
</p>

## Feature Covered:
<ul>
  <li><h3>Required by BS23</h3>
  <ol>
  <li>Fetch Flutter repositories through Github API : <b>Retrofit(auto generated)</b>
  </li>
  <li>Fetched data stored in local sqlite database for offline mode: <b>Floor(auto generated)</b></li>
  <li>Paginated data featched from remote and show paginated data on scrolling: <b>InfiniteScrollPagination</b>
  </li>
  <li>Can Refresh repository list screen once every 6 seconds: <b>Pull to Refresh</b></li>
  <li>Show list of flutter repositories on home screen</b></li>
  <li>List can be sorted by star count & sorting will persists in further app sessions: <b>Shared Preference</b></li>
  <li>Repository details screen with repository owner's data</b></li>
  <li>The repository list and details screen data saved to local sqlite database for offline access</li>
</ol>
  </li>
  <li><h3>Non Required</h3>
   <ol>
     <li>Flutter version Management <a href="https://fvm.app/documentation/getting-started"><b>fvm</b></a></b>
  <li>Launch screen with app icon</b>
  </li>
  <li>Initial loader screen</li>
  <li>Shimmer loader for list screen
  </li>
  <li>Use of network interceptor </li>
  <li>Network alert pop up</li>
 
</ol>
</li>
  <li><h3>Extra Credit</h3>
  <ol>
  <li>
Unit Testing for homeViewModel's paginated api call: <b>Mockito</b>
  </li>
  <li>
State Management tool: <a href="https://stacked.filledstacks.com/docs/getting-started/overview/"><b>Stacked</b></a>
  </li>
  <li>
Dependency Injection: <b>Stacked,GetX,Injectable</b>
  </li>
  <li>
Repository patten with abstraction layer in (data,domain)</b>
  </li>
  <li>
Central network call error handling via network interceptor</b>
  </li>
   <li>
Simple Api environment flavor (dev,prod) add</b>
  </li>


  </ol>
  </li>
</ul>

## Technologies & patterns:
<ul>
<li>
App Skeleton:<h3>Clean Architecture</h3> 
</li>
<li>
presentation(UI & Business):<h3>MVVM with stacked Framework</h3> 
</li>
<li>
Api integrate<h3>Retrofit</h3> 
</li>
<li>
Local Database<h3>sqlite(floor)</h3> 
</li>
<li>
Dependency Injection<h3>GetX,stacked,Injectable</h3> 
</li>
<li>
Data Model parsing <h3>json_serializable</h3> 
</li>
<li>
Unit testing<h3>Mockito with stacked Framework</h3> 
</li>
<li>
Flutter version Management<h3><a href="https://fvm.app/documentation/getting-started"><b>fvm</b></a></h3> 
</li>

</ul>
